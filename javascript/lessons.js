
function Lesson(title,description,roomNumber,lecturerName,time,date,lecturerEmail){
this.title = title,
this.description = description;
this.roomNumber = roomNumber;
this.lecturerName = lecturerName;
this.time = time;
this.date = date;
this.lecturerEmail = lecturerEmail;
}

var lessons =[];
var calculus = new Lesson
	("Calculus",
	" Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod " ,
	chooseRoomNumber(),
	faker.name.findName(),
	chooseRandomTime(),
	faker.date.weekday(),
	faker.internet.email());

var trigonometry = new Lesson
	("Trigonometry",
	"Trigonometry (from Greek trigōnon, triangle and metron, measure is a branch of mathematics that studies relationships involving lengths and angles of triangles. The field emerged in the Hellenistic world during the 3rd century BC from applications of geometry to astronomical studies " ,
	chooseRoomNumber(),
	faker.name.findName(),
	chooseRandomTime(),
	faker.date.weekday(),
	faker.internet.email());

	var matrices = new Lesson
	("Matrices",
	"Learn what matrices are and about their various uses: solving systems of equations, transforming shapes and vectors, and representing real-world situations. Learn how to add, subtract, and multiply matrices, and find the inverses of matrices. " ,
	chooseRoomNumber(),
	faker.name.findName(),
	chooseRandomTime(),
	faker.date.weekday(),
	faker.internet.email());

lessons.push(calculus);
lessons.push(trigonometry);
lessons.push(matrices);

function BuildLessonBlock(){
	for(var i=0;i < lessons.length;i++){
	 $("#lessonList").append("<div class='lesson col-lg-12'>" +
	  "<h3 class='subjectTitle col-lg-8'>" + lessons[i].title + "</h3>" + "<h3 class='col-lg-4'>Details</h3>"
	   + "<div class='lessonDescription col-lg-8 col-md-12'>" + lessons[i].description + "</div>" +
	   BuildList(lessons[i]) + "</div>");
		}
}

function BuildList(obj){
	var str = "<div class='lessonDetails col-lg-4 col-md-12'>" 
	 	+ "<ul><li>"+ obj.roomNumber+"</li>" +
	 	"<li>"+ obj.lecturerName+"</li>" +
	 	"<li>"+ obj.time+"</li>" +
	 	"<li>"+ obj.date+"</li>" +
	 	"<li>"+ obj.lecturerEmail+"</li>" 
	 	+" </ul>" + 
	 	"</div>";
	 	return str;
}

BuildLessonBlock();
doSomething();

function chooseRoomNumber(){
	var rand = Math.floor(Math.random() * 2);
	if(rand === 1){
		return "P1101";
	}else{
		return"P1102";
	}
}

function chooseRandomTime(){
	var rand = Math.floor(Math.random() * (18-9+1)+9);
	return rand + ":00";
}

